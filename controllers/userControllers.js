const User = require('../models/User');

module.exports.createUserContollers  = (req,res) => 
{
    console.log(req.body);

    User.findOne({username: req.body.username})
    .then(result => {
        if(result !== null && result.username === req.body.username)
        {
            return res.send('Duplicate Task found')
        }
        else
        {
            let newUser= new User ({
                username: req.body.username,
                password: req.body.password
            })

            newUser.save()
            .then(result => res.send(result))
            .catch(error => res.send(error));
        }
    })

    .catch(error => res.send(error))
};


module.exports.getAllUserController = (req,res) => {

    User.find({})
    .then(result => res.send(result))
    .catch(error => res.send(error))
};

//UPDATE SINGLE  USER's USERNAME
module.exports.updateUsernameController =  (req,res)  =>
{
    console.log(req.params.id);
    console.log(req.body)

    let usernameUpdates = {
        username: req.body.username
    }
    // {new:true} = allows us to return the updated version of the document we were updating, By default, without  this argument, findByIdAndUpdate will  return  the previous state of the document or previous version
    User.findByIdAndUpdate(req.params.id, usernameUpdates, {new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}


module.exports.getSingleUserController  = (req,res) => 
{
    console.log(req.params);

    
    let id = req.params.id

    User.findById(id)
    .then(result =>  res.send(result))
    .catch(error  => res.send(error))
};
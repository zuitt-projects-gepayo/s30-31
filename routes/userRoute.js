// importing express to use the Router() method
const express =require('express');

//allows us to access our HTTP method routes
const router = express.Router();

//importing taskControllers
const userControllers = require('../controllers/userControllers');

//creating user
router.post("/", userControllers.createUserContollers);

//retrieveing  all user route
router.get("/", userControllers.getAllUserController);

// retrieve single user by ID
router.get("/getSingleUser/:id", userControllers.getSingleUserController);

//updating single task's route
router.put("/:id", userControllers.updateUsernameController);

module.exports = router;

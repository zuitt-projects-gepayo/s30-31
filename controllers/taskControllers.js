const Task = require('../models/Task');

//CREATE  NEW  TASK
module.exports.createTaskContollers  = (req,res) => 
{
    console.log(req.body);

    Task.findOne({name: req.body.name})
    .then(result => {
        if(result !== null && result.name === req.body.name)
        {
            return res.send('Duplicate Task found')
        }
        else
        {
            let newTask= new Task ({
                name: req.body.name,
                status: req.body.status
            })

            newTask.save()
            .then(result => res.send(result))
            .catch(error => res.send(error));
        }
    })

    .catch(error => res.send(error))
};

//  GET ALL  TASK
module.exports.getAllTaskController = (req,res) => {

    //db.tasks.find({})
    Task.find({})
    .then(result => res.send(result))
    .catch(error => res.send(error))
};

//GET  SINGLE  TASK VIA ID
module.exports.getSingleTaskController  = (req,res) => 
{
    console.log(req.params);

    /* you can do this
    // let id = req.params.id
    // Task.findByI(id)
    */
    Task.findById(req.params.id)
    .then(result =>  res.send(result))
    .catch(error  => res.send(error))
};


//UPDATE SINGLE TASK STATUS VIA ID
module.exports.updateTaskStatusController =  (req,res)  =>
{
    console.log(req.params.id);
    console.log(req.body)

    let updates = {
        status: req.body.status
    }
    // {new:true} = allows us to return the updated version of the document we were updating, By default, without  this argument, findByIdAndUpdate will  return  the previous state of the document or previous version
    Task.findByIdAndUpdate(req.params.id, updates, {new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}




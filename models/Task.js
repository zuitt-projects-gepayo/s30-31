
const mongoose = require('mongoose');

//Mongoose Schema
    //before we can create documents from our API to save into our database, we first have to determine the structure of the documents to be written i nthe database

    //  A schema is a blueprint for ourdocument

    //Schema() is a constructorfrom mongoose that will allow us to create a new schema object

const taskSchema = new mongoose.Schema(

    /*
        define the field forour task document.
        The task document should thave the name and status fields
        Both of the field should have a data  type of string
    */
    {
        name: String,
        status: String,
    }
);

//Mongoosemodel
    //models are use to connect your API to the corrensponding collection in your database. it isa representation of the task document.

    // models used schema to create objects that correspond toschema. by default, when creating the collectionfromyour model, the collection name is pluralized

    //SYNTAX: mongoose.model(<nameOfCollectionInAtlas>, <schemaToFollow>)
module.exports = mongoose.model("Task", taskSchema);
//module.exports will allow us to import/ require them inanother file within our application
//export the model intor other files that may require it
//NOTE: all packages / modules should be requireed at the top of the file to avoid tampering or errors

const express = require('express'); // for the server

// Mongoose is a package that uses an ODM or object document mapper. it allows us to translate our JS objects into database documents for MongoDB. It allows connection and easier manipulation of our documents in MongoDB
const mongoose = require('mongoose'); // for the database

const taskRoutes =require('./routes/taskRoute');

const userRoutes = require('./routes/userRoute');

const port = 4000;

const app = express();

/*
    Syntax:
        mongoose.connect("<connectionStringFromMongoDBAtlas>", {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
*/

mongoose.connect("mongodb+srv://admin_gepayo:admin169@gepayo-169.td1lc.mongodb.net/task169?retryWrites=true&w=majority", 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
//Create notification if the connection to the db is successful or not

let db = mongoose.connection;

//we add this si that when db has a connection error, we will show the connection error both in the terminal and in the browser for our client
db.on('error', console.error.bind(console, 'Connection Error'));

//once the connection is open and successful, we will output a message in the terminal
db.once('open', () => console.log('Connected to MongoDB'));

app.use(express.json())

app.use('/tasks', taskRoutes);

app.use('/users',userRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`))
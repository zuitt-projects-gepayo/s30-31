// importing express to use the Router() method
const express =require('express');

//allows us to access our HTTP method routes
const router = express.Router();

//importing taskControllers
const taskControllers = require('../controllers/taskControllers');

//creating task
router.post("/", taskControllers.createTaskContollers);

//retrieveingall task route
router.get("/", taskControllers.getAllTaskController);

//retrieving single task  route
// router.get("/getSingleTask/:id", taskControllers.getSingleTaskController);

//updating single task's route
router.put("/updateTaskStatus/:id", taskControllers.updateTaskStatusController)

module.exports = router;
